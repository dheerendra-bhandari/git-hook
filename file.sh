
#!/bin/sh

# Run ESLint on staged files
FILES=$(git diff --cached --name-only | grep '\.js$')
if [ "$FILES" = "" ]; then
    exit 0
fi

echo "Running ESLint on staged files:"

PASS=true

for FILE in $FILES
do
    eslint "$FILE"
    if [ "$?" -ne 0 ]; then
        PASS=false
    fi
done

echo ""
if ! $PASS; then
    echo "ESLint check failed. Please fix errors before committing."
    exit 1
else
    echo "ESLint check passed. Ready to commit."
    exit 0
fi
             
